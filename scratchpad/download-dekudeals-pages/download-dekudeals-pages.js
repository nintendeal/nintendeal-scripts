'use strict';

const axios = require('axios');
const cheerio = require('cheerio');
const fs = require('fs');

let dekuGames = fs.readFileSync('game-urls', {encoding:'utf8', flag:'r'}).split('\n');

const dekuDealsUrl = 'https://www.dekudeals.com';

let count = 1;

main();

async function main() {
  for (const game of dekuGames) {
    console.log(count);
    await getGames(game);
    count = count + 1;
  }
}

async function getGames(game) {
  const gameUrl = dekuDealsUrl + game;
  console.log(game.split("/items/")[1] + "\n");
  axios.get(gameUrl)
  .then((response) => {
    fs.appendFileSync('./games/' + game.split("/items/")[1], response.data);
  });
  await wait(300);
}

async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}