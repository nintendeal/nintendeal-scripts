'use strict';

const axios = require('axios');
const cheerio = require('cheerio');
const { promises: fs } = require("fs");
const util = require("util");
const mongoose = require('mongoose');
const { create } = require('domain');
const readdir = util.promisify(fs.readdir);

const mongooseConnectionString = 'mongodb://dev:qnx75me6qspl3xo5@SG-nintendb-35543.servers.mongodirector.com:27017/nintendeal';
mongoose.connect(mongooseConnectionString, { useNewUrlParser: true, useUnifiedTopology: true });

mongoose.connection.on("connected", function() {
  console.log("Connected to database");
});

mongoose.connection.on("error", function(error) {
  console.log("Connection to database failed:" + error);
});

mongoose.connection.on("disconnected", function() {
  console.log("Disconnected from database");
});

var gameSchema = new mongoose.Schema({
  eshop_id: String,
  title: String,
  url: String,
  description: String,
  slug: String,
  box_art: String,
  release_date: String,
  esrb: String,
  virtual_console: String,
  locale: String,
  players: String,
  price_range: String,
  nsuid: String,
  developers: String,
  publishers: String,
  platform: String,
  type: String,
  categories: [String],
  screenshots: [String],
  missing_page: Boolean,
}, {collection: 'games'
});

var amazonAffiliateSchema = new mongoose.Schema({
  slug: String,
  company: String,
  game_type: String,
  asin: String,
  url: String,
  active: Boolean
}, {collection: 'games_affiliates'
});

let count = 1;

setTimeout( () => {
  main();
}, 2000);

async function main() {
  const gameModel = mongoose.model('Game', gameSchema);
  const affiliateModel = mongoose.model('AmazonAffiliateSchema', amazonAffiliateSchema);
  let gameFiles = await getGameFiles();
  await asyncForEach(gameFiles, async (file) => {
    const game = await gameModel.find({"slug": file + '-switch'});
    if (game.length === 0) {
      //fs.appendFile('./nomatch.log', file + '-switch\n');
    } else {
      //console.log("Found game " + file + "-switch");
      //fs.appendFile('./match.log', file + '-switch\n');
      console.log("[" + count + "] " + file + '-switch');
      await processScrapedData(file, affiliateModel);
      await wait(200);
    }
    count++;
  });
  //await wait(5000);
  //mongoose.connection.close()
}

async function processScrapedData(file, affiliateModel) {
  const scrapeData = await fs.readFile("games/" + file);
  const $ = cheerio.load(scrapeData);
  $('.item-price-table tr a').map( async (index, element) => {
    if (($(element)['0'].attribs.href).includes("amazon.com") && 
      $(element)['0'].children[0].data == "\nPhysical\n") {
      let amazonUrl = $(element)['0'].attribs.href.split("?tag=dekudeals-20")[0];
      let split = amazonUrl.split("/dp/");
      let asin = split[1];
      console.log(asin + ": Physical");
      //console.log($(element)['0'].children[0].data);
      await createAffiliateLink(affiliateModel, file, "Amazon", "Physical", asin, amazonUrl, true);
    }

    if (($(element)['0'].attribs.href).includes("amazon.com") && 
      $(element)['0'].children[0].data == "\nDigital\n") {
      
      let amazonUrl = $(element)['0'].attribs.href.split("?tag=dekudeals-20")[0];
      let split = amazonUrl.split("/dp/");
      let asin = split[1];
      console.log(asin + ": Digital");
      //console.log($(element)['0'].children[0].data);
      await createAffiliateLink(affiliateModel, file, "Amazon", "Digital", asin, amazonUrl, true);
    }
    
  });
}

async function createAffiliateLink(affiliateModel, slug, company, game_type, asin, amazonUrl, active) {
  const affiliate = new affiliateModel({
    slug: slug,
    company: company,
    game_type: game_type,
    asin: asin,
    url: amazonUrl,
    active: active
  });
  affiliate.save();
}

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

async function getGameFiles() {
  let names;
  try {
      names = await fs.readdir("./games/");
  } catch (e) {
      console.log("e", e);
  }
  return names;
}