'use strict';

const axios = require('axios');
const cheerio = require('cheerio');
const fs = require('fs');

const dekuDealsGamePages = 'https://www.dekudeals.com/games?page=';
const numOfPages = 148;

main();

async function main() {
  await getGames();
}

async function getGames() {
  for (let page = 1; page <= numOfPages; page++) {
    console.log(page);
    axios.get(dekuDealsGamePages + page)
    .then((response) => {
      const $ = cheerio.load(response.data);
      $('.main-link').map( async (index, element) => {
        console.log(element.attribs.href);
        fs.appendFileSync('./game-urls', element.attribs.href + '\n');
      });
    });
    await wait(2000);
  }  
}

async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}