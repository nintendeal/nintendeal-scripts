'use strict';

const amazonPaapi = require('amazon-paapi');
const mongoose = require('mongoose');
const readline = require('readline-sync');
const Spinner = require('cli-spinner').Spinner;

const spinner = new Spinner('%s');
spinner.setSpinnerString("◢◣◤◥");
console.clear();

const mongooseConnectionString = 'mongodb://dev:qnx75me6qspl3xo5@SG-nintendb-35543.servers.mongodirector.com:27017/nintendeal';
mongoose.connect(mongooseConnectionString, { useNewUrlParser: true, useUnifiedTopology: true });

mongoose.connection.on("connected", function() {
  console.log("Connected to database");
});

mongoose.connection.on("error", function(error) {
  console.log("Connection to database failed:" + error);
});

mongoose.connection.on("disconnected", function() {
  console.log("Disconnected from database");
});

var gameSchema = new mongoose.Schema({
  eshop_id: String,
  title: String,
  url: String,
  description: String,
  slug: String,
  box_art: String,
  release_date: String,
  esrb: String,
  virtual_console: String,
  locale: String,
  players: String,
  price_range: String,
  nsuid: String,
  developers: String,
  publishers: String,
  platform: String,
  type: String,
  categories: [String],
  screenshots: [String],
  missing_page: Boolean,
}, {collection: 'games_backup'
});

const commonParameters = {
  'AccessKey' : 'AKIAJ4NG6O5RS7EOLFMA',
  'SecretKey' : 'YOhVxi7Vob5+1fPmS6vz0XtnShr28UkDkTUCDl6y',
  'PartnerTag' : 'n1ndw3b-20', // yourtag-20
  'PartnerType': 'Associates', // Optional. Default value is Associates.
  'Marketplace': 'www.amazon.com' // Optional. Default value is US.
};


setTimeout( () => {
  main();
}, 2000);

async function main () {
  const requestParameters = {
    'Keywords' : `Kirby nintendo switch`,
    'SearchIndex' : 'VideoGames',
    'ItemCount' : 3,
    'Resource': [
        'Images.Primary.Medium',
        'ItemInfo.Title', 
        'Offers.Listings.Price']
  };

  const gameModel = mongoose.model('Game', gameSchema);
  const results = await gameModel.find({}, {title: 1});
  let count = 1;
  for (const game of results) {
    if (count === 20) {
      break;
    }
    const gameTitle = game['title'];
    const requestParameters = {
      'Keywords' : `${gameTitle} nintendo switch`,
      'SearchIndex' : 'VideoGames',
      'ItemCount' : 5,
      'Resource': [
          'Images.Primary.Medium',
          'ItemInfo.Title', 
          'Offers.Listings.Price']
    };

    function timeout(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
    }

    spinner.start();
    await timeout(250);

    /** Promise */
    await amazonPaapi.SearchItems(commonParameters, requestParameters)
        .then(data => {
          spinner.stop();
          console.clear();
            // do something with the success response.
            console.log('\x1b[41m\x1b[37m  [' + count + ' of ' + results.length + ']  \x1b[0m\n');

            console.log('\x1b[43m\x1b[30m  Game: ' + gameTitle + '  \x1b[0m');
            console.log('Searching amazon for: \x1b[36m' + gameTitle + ' nintendo switch\x1b[0m\n');
            let asins = [];
            if (data.SearchResult !== undefined) {
              console.log('1. ' + data.SearchResult.Items[0].ItemInfo.Title.DisplayValue + ' (http://amazon.com/dp/' + data.SearchResult.Items[0].ASIN + ')\n');
              if (data.SearchResult.TotalResultCount > 1) {
                console.log('2. ' + data.SearchResult.Items[1].ItemInfo.Title.DisplayValue + ' (http://amazon.com/dp/' + data.SearchResult.Items[1].ASIN + ')\n');
              }

              if (data.SearchResult.TotalResultCount > 2) {
                console.log('3. ' + data.SearchResult.Items[2].ItemInfo.Title.DisplayValue + ' (http://amazon.com/dp/' + data.SearchResult.Items[2].ASIN + ')\n');
              }

              if (data.SearchResult.TotalResultCount > 3) {
                console.log('4. ' + data.SearchResult.Items[3].ItemInfo.Title.DisplayValue + ' (http://amazon.com/dp/' + data.SearchResult.Items[3].ASIN + ')\n');
              }

              if (data.SearchResult.TotalResultCount > 4) {
                console.log('5. ' + data.SearchResult.Items[4].ItemInfo.Title.DisplayValue + ' (http://amazon.com/dp/' + data.SearchResult.Items[4].ASIN + ')\n');
              }

              let choice = readline.question("\x1b[32mSelect correct amazon links:\x1b[0m ");
              if (choice.length > 0) {
                let selection = choice.split(',');
                for (let s = 0; s < selection.length; s++) {
                  asins.push(data.SearchResult.Items[selection[s] - 1].ASIN);
                }
              }

            } else {
              console.log('No results found.');
              readline.question("\x1b[31mPress enter to continue\x1b[0m ");
            }

            console.log('\nUploading ASINS \x1b[42m\x1b[30m[ ' + asins + ' ]\x1b[0m');

        })
        .catch(error => {
            // catch an error.
            console.log(error)
        });
        await timeout(1000);
        console.clear();
      count++;
    }
    mongoose.connection.close();
};