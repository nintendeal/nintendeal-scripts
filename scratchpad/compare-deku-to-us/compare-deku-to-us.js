'use strict'; 

const fs = require('fs');

let ourNsuidsMap = new Map();

let dekuGames = fs.readFileSync('games-urls.log', {encoding:'utf8', flag:'r'}).split('\n');
let ourSlugs = fs.readFileSync('slugs', {encoding:'utf8', flag:'r'}).split('\n');

for (const game of ourSlugs) {
  ourNsuidsMap.set(game, true);
}

for (const game of dekuGames) {
  const parsed = game.split(" : /items/")[1];
  console.log(parsed + '-switch');
  if (!ourNsuidsMap.has(parsed + '-switch')) {
    fs.appendFileSync('./newslugs', parsed + '-switch'+'\n');
  }
}