'use strict'; 

const fs = require('fs');

let ourNsuidsMap = new Map();

let eshopNsuids = fs.readFileSync('game_nsuids', {encoding:'utf8', flag:'r'}).split('\n');
let ourNsuids = fs.readFileSync('nsuids', {encoding:'utf8', flag:'r'}).split('\n');

for (const game of ourNsuids) {
  ourNsuidsMap.set(game, true);
}

for (const game of eshopNsuids) {
    if (game.indexOf('/') !== 0) {
        console.log(game);
        if (!ourNsuidsMap.has(game)) {
          fs.appendFileSync('./newnsuids', game+'\n');
        }
    }
}