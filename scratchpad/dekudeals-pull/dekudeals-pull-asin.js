'use strict';

const fs = require('fs');
const axios = require('axios');
const cheerio = require('cheerio');
const { performance } = require('perf_hooks');
const mongoose = require('mongoose');

const mongooseConnectionString = 'mongodb://dev:qnx75me6qspl3xo5@SG-nintendb-35543.servers.mongodirector.com:27017/nintendeal';
mongoose.connect(mongooseConnectionString, { useNewUrlParser: true, useUnifiedTopology: true });

mongoose.connection.on("connected", function() {
  console.log("Connected to database");
});

mongoose.connection.on("error", function(error) {
  console.log("Connection to database failed:" + error);
});

mongoose.connection.on("disconnected", function() {
  console.log("Disconnected from database");
});

var gameSchema = new mongoose.Schema({
    eshop_id: String,
    title: String,
    url: String,
    description: String,
    slug: String,
    box_art: String,
    release_date: String,
    esrb: String,
    virtual_console: String,
    locale: String,
    players: String,
    price_range: String,
    nsuid: String,
    developers: String,
    publishers: String,
    platform: String,
    type: String,
    categories: [String],
    screenshots: [String],
    missing_page: Boolean,
    asins: [String],
    asins_urls: [String],
  }, {collection: 'games'
  });

  const GET_ALL_GAMES_ADDRESS = 'https://www.dekudeals.com/games?page_size=all';
  const WEBSITE_ADDRESS = 'https://www.dekudeals.com';
  
  setTimeout( () => {
    main();
  }, 2000);

  async function main() {
    //let startTime = performance.now();

    //let rawdata = fs.readFileSync(`allgames`, {encoding:'utf8', flag:'r'});
    //await processGamePages(rawdata);

    let gamesUrls = fs.readFileSync('games-urls.log', {encoding:'utf8', flag:'r'}).split('\n');
    let count = 0;
    for (const game of gamesUrls) {
      console.log(count);
      setTimeout( () => {
        getASIN(game);
      }, 2000);
      count++;
      if (count == 75) {
        break;
      }
    }



    //let finishTime = performance.now();
    //let elapsedTime = ((finishTime - startTime) / 1000) / 60;
    //console.log(`\nDone. Took ${elapsedTime} minutes.`);
    //mongoose.connection.close();
  }

  async function getASIN(game) {
    // const gameModel = mongoose.model('Game', gameSchema);
    // if (await gameModel.exists({ slug: slug })) {
      
    // } else {
    //   console.log(slug);
    // }

    await axios.get(WEBSITE_ADDRESS + game.split(' : ')[1])
    .then((response) => {
      //console.log(game.split(' : ')[0])
      const $ = cheerio.load(response.data);
      let slug = '';
      $('.eshop.logo').map( async (index, element) => {
        slug = (element.parent.attribs.href).split('https://www.nintendo.com/games/detail/')[1];
      });
      $('.amazon_com.logo').map( async (index, element) => {
        //console.log(element.parent.attribs.href);
        const gameModel = mongoose.model('Game', gameSchema);
        if (await gameModel.exists({ slug: slug })) {

        } else if (await gameModel.exists({ slug: (game.split(' : ')[1]).split('/items/')[1] })) {

        } else {
          console.log(slug);
        }
      });
    })

  }

  async function processGamePages(scrapeData) {
    const $ = cheerio.load(scrapeData);
    let counter = 1;
    $('.row.item-grid2 .col-xl-2.col-lg-3.col-sm-4.col-6.cell .main-link .h6.name').map( async (index, element) => {
      const dekuGame = ((element.children[0].data).replace(/(\r\n|\n|\r)/gm, "") + ' : ' + (element.parent.attribs.href).replace(/(\r\n|\n|\r)/gm, ""));
      console.log(dekuGame);
      fs.appendFileSync('./games-urls.log', dekuGame + '\n');
      counter++;
    });
    console.log(counter);
  }