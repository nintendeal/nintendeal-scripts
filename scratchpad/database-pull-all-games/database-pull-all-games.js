'use strict'; 

const fs = require('fs');
const mongoose = require('mongoose');

const mongooseConnectionString = 'mongodb://dev:qnx75me6qspl3xo5@SG-nintendb-35543.servers.mongodirector.com:27017/nintendeal';
mongoose.connect(mongooseConnectionString, { useNewUrlParser: true, useUnifiedTopology: true });

mongoose.connection.on("connected", function() {
  console.log("Connected to database");
});

mongoose.connection.on("error", function(error) {
  console.log("Connection to database failed:" + error);
});

mongoose.connection.on("disconnected", function() {
  console.log("Disconnected from database");
});

var gameSchema = new mongoose.Schema({
  eshop_id: String,
  title: String,
  url: String,
  description: String,
  slug: String,
  box_art: String,
  release_date: String,
  esrb: String,
  virtual_console: String,
  locale: String,
  players: String,
  price_range: String,
  nsuid: String,
  developers: String,
  publishers: String,
  platform: String,
  type: String,
  categories: [String],
  screenshots: [String],
  missing_page: Boolean,
}, {collection: 'games'
});

async function pullAllGames() {
  const gameModel = mongoose.model('Game', gameSchema);

  for await (const game of gameModel.find()) {
    fs.appendFileSync('./slugs', game['slug']+'\n');
  }
  mongoose.connection.close();
}

pullAllGames();