'use strict';

const axios = require('axios');
const cheerio = require('cheerio');
const fs = require('fs');

const eshopPricesMainAddress = 'https://eshop-prices.com';
const eshopPricesGamesAddress = 'https://eshop-prices.com/games?page=';
const numOfPages = 173;

main();

async function main() {
  //await getGameUrls();
  await getNSUIDs();
}


async function getGameUrls() {
  for (let page = 1; page <= numOfPages; page++) {
    axios.get(eshopPricesGamesAddress + page)
    .then((response) => {
      const $ = cheerio.load(response.data);
      $('.games-list-item').map( async (index, element) => {
        console.log(element.attribs.href);
        fs.appendFileSync('./game_urls', element.attribs.href + '\n');
      });
    });
  }  
}

async function getNSUIDs() {
  let gamesUrls = fs.readFileSync('./game_urls', {encoding:'utf8', flag:'r'}).split('\n');

  for (const game of gamesUrls) {
    await wait(2000);
    await call(game);
  }
}

async function call(game) {
  console.log(game);
  const config = {
    headers: {'Host': 'eshop-prices.com', 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:77.0) Gecko/20100101 Firefox/77.0', 'X-Requested-With': 'XMLHttpRequest'}
  }
  axios.get(eshopPricesMainAddress + game + '/buy?store=115', config)
  .catch(() => { fs.appendFileSync('./game_nsuids', game + '\n'); })
  .then((response) => {
    const $ = cheerio.load(response.data);
    $('a').map( async (index, element) => {
      if (element.attribs.href.indexOf('https://ec.nintendo.com') > -1) {
        console.log(element.attribs.href.split('https://ec.nintendo.com/title_purchase_confirm?title=')[1]);
        fs.appendFileSync('./game_nsuids', element.attribs.href.split('https://ec.nintendo.com/title_purchase_confirm?title=')[1] + '\n');
      }
    });
  });
}

async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}