import requests
import json
import time
import urllib.parse
import os

base_address = 'https://u3b6gr4ua3-dsn.algolia.net'
relative_path = '/1/indexes/*/queries'
query_strings = ('?x-algolia-agent=Algolia%20for%20JavaScript%20(3.33.0)'
                '%3B%20Browser%20(lite)%3B%20JS%20Helper%202.20.1&'
                'x-algolia-application-id=U3B6GR4UA3&x-algolia-api-key='
                'c4da8be7fd29f0f5bfa42920b0a99dc7')
url = "{0}{1}{2}".format(base_address, relative_path, query_strings)

headers = {
'Content-Type': 'application/x-www-form-urlencoded',
'Accept': 'application/json'
}

def main():
    os.mkdir(os.path.join(os.getcwd(), "games/crawler/az"))
    for page in range(24):
        json_file_name = "./games/crawler/az/{0}.json".format(str(page))
        json_file = open(json_file_name,"w")
        data_to_json = []
        post_payload = get_post_payload(page)

        json_response = (requests.request("POST", url, 
                        headers=headers, 
                        data = post_payload)).text.encode('utf8')
        json_decoded = json.loads(json_response)
        print("Games Found: {0}".format(str(len(json_decoded['results'][0]['hits']))))
        
        data_to_json.append(json_decoded['results'][0]['hits'])

        json_file.write(json.dumps(data_to_json))
        json_file.close()

def get_post_payload(page):
    return "{\"requests\":[{\"indexName\":\"ncom_game_en_us_title_asc\"" \
        ",\"params\":\"query=&hitsPerPage=42&maxValuesPerFacet=30&page=" \
        + str(page) + \
        "&analytics=false&facets=%5B%22generalFilters%22%2C%22platform" \
        "%22%2C%22availability%22%2C%22genres%22%2C%22howToShop%22%2C%22" \
        "virtualConsole%22%2C%22franchises%22%2C%22priceRange%22%2C%22esrbRating" \
        "%22%2C%22playerFilters%22%5D&tagFilters=&facetFilters=%5B%5B%22" \
        "availability%3AAvailable%20now%22%5D%2C%5B%22platform%3ANintendo%20" \
        "Switch%22%5D%5D\"},{\"indexName\":\"ncom_game_en_us_title_asc\",\"" \
        "params\":\"query=&hitsPerPage=1&maxValuesPerFacet=30&page=0&analytics" \
        "=false&attributesToRetrieve=%5B%5D&attributesToHighlight=%5B%5D&" \
        "attributesToSnippet=%5B%5D&tagFilters=&facets=availability&facetFilters" \
        "=%5B%5B%22platform%3ANintendo%20Switch%22%5D%5D\"},{\"indexName\":\"" \
        "ncom_game_en_us_title_asc\",\"params\":\"query=&hitsPerPage=1&" \
        "maxValuesPerFacet=30&page=0&analytics=false&attributesToRetrieve=" \
        "%5B%5D&attributesToHighlight=%5B%5D&attributesToSnippet=%5B%5D&" \
        "tagFilters=&facets=platform&facetFilters=%5B%5B%22availability" \
        "%3AAvailable%20now%22%5D%5D\"}]}"

if __name__ == '__main__':
    main()