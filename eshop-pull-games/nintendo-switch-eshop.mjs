import pkg from 'nintendo-switch-eshop';
import {writeFileSync } from 'fs';

const games = await pkg.getGamesAmerica();

let filenameCount = 1;
let count = 0;
let gamesData = [];

for (const game of games) {
  gamesData.push(game);
  count++;
  if (count === 200) {
    const toWrite = JSON.stringify(gamesData);
    writeFileSync(`./games/nintendo-switch-eshop-api/${filenameCount}.json`, "[" + toWrite + "]");
    console.log(`${filenameCount}.json has been saved!`);
    gamesData = [];
    count = 0;
    filenameCount++;
  }
}