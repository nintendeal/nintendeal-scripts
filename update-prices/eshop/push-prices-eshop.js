'use strict';

const axios = require('axios');
const { promises: fs } = require("fs");
const util = require("util");
const mongoose = require('mongoose');

const mongooseConnectionString = 'mongodb://dev:qnx75me6qspl3xo5@SG-nintendb-35543.servers.mongodirector.com:27017/nintendeal';
mongoose.connect(mongooseConnectionString, { useNewUrlParser: true, useUnifiedTopology: true });

mongoose.connection.on("connected", function() {
  console.log("Connected to database");
});

mongoose.connection.on("error", function(error) {
  console.log("Connection to database failed:" + error);
});

mongoose.connection.on("disconnected", function() {
  console.log("Disconnected from database");
});

var gameSchema = new mongoose.Schema({
  eshop_id: String,
  title: String,
  url: String,
  description: String,
  slug: String,
  box_art: String,
  release_date: String,
  esrb: String,
  virtual_console: String,
  locale: String,
  players: String,
  price_range: String,
  nsuid: String,
  developers: String,
  publishers: String,
  platform: String,
  type: String,
  categories: [String],
  screenshots: [String],
  missing_page: Boolean,
}, {collection: 'games'
});

var eshopPricePoint = new mongoose.Schema({
  slug: String,
  date: Date,
  price: Number
}, {collection: 'games_prices_eshop'
});

const eshopPriceEndpoint = 'https://api.ec.nintendo.com/v1/price?country=US&lang=en&ids=';

setTimeout( () => {
  main();
}, 2000);

async function main() {
  const gameModel = mongoose.model('Game', gameSchema);
  const eshopPriceModel = mongoose.model('Price', eshopPricePoint);
  
  let count = 1;
  for await (const game of gameModel.find({})) {
    await axios.get(eshopPriceEndpoint + game.nsuid)
      .then(async (response) => {
        if (response != undefined && response.data != undefined) {
          const rawPrice = await getPrice(response.data);
          if (rawPrice !== null) {
            const currentPrice = parseFloat(rawPrice);
            console.log("[" + count + "] " + game.title + " : " + currentPrice);
            const price = new eshopPriceModel({
              slug: game.slug,
              date: new Date(),
              price: currentPrice
            });
            price.save();
          }
        }
      })
      .catch((ex) => {
        console.log(ex);
      });
    count++;
    await wait(50);
  }
  mongoose.connection.close()
}


async function getPrice(data) {
  try {
    let price = data.prices[0].regular_price.raw_value
    return price;
  } catch (ex) {
    return null;
  }
}

async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}