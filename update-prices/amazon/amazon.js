'use string';

const amazon = require('amazon-paapi');
const util = require('util');
const mongoose = require('mongoose');

const mongooseConnectionString =
  'mongodb://dev:qnx75me6qspl3xo5@SG-nintendb-35543.servers.mongodirector.com:27017/nintendeal';
mongoose.connect(mongooseConnectionString, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

mongoose.connection.on('connected', () => {
  console.log(`Connected to database`);
});

mongoose.connection.on('error', (error) => {
  console.log(`Connection to database failed: ${error}`);
});

mongoose.connection.on('disconnected', () => {
  console.log(`Disconnected from database`);
});

var amazonAffiliateSchema = new mongoose.Schema({
  slug: String,
  company: String,
  game_type: String,
  asin: String,
  url: String,
  active: Boolean
}, {collection: 'games_affiliates'
});

let amazonPriceSchema = new mongoose.Schema(
  {
    slug: String,
    date: Date,
    price: Number,
    asin: String
  },
  { collection: 'games_prices_amazon' }
);

const commonParameters = {
  AccessKey: 'AKIAI2HDUZTP4IEA4VYQ',
  SecretKey: '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',
  PartnerTag: 'n1ndw3b-20',
  PartnerType: 'Associates',
  Marketplace: 'www.amazon.com',
};



const main = async () => {
  try {
    console.clear();

    const amazonPriceModel = mongoose.model('Price', amazonPriceSchema);
    const affiliateModel = mongoose.model('AmazonAffiliateSchema', amazonAffiliateSchema);
    
    let pricePromises = [];
    let count = 0;

    for await (const gameAffiliate of affiliateModel.find({'company': 'Amazon'})) {
      const requestParameters = {
        ItemIds: [gameAffiliate['asin']],
        ItemIdType: 'ASIN',
        Condition: 'New',
        Resources: ['ItemInfo.Title', 'Offers.Listings.Price'],
      };
      
      try {
        amazon.GetItems(commonParameters, requestParameters)
        .then((response) => {
          if (response != null && response != undefined &&
            response.ItemsResult !== null && response.ItemsResult !== undefined &&
            response.ItemsResult.Items !== null && response.ItemsResult.Items !== undefined) {
            response.ItemsResult.Items.forEach(async (item) => {
              if (item.Offers != undefined && item.Offers.Listings != undefined
                  && item.Offers.Listings[0].Price != undefined) {
                    count = count + 1;
                pricePromises.push(new Promise(async (resolve, reject) => {
                  const dataPrice = item.Offers.Listings[0].Price.Amount;
                  console.log(`[${count}] ${gameAffiliate.slug} (${gameAffiliate.asin}) : ${dataPrice}`);
          
                  const price = new amazonPriceModel({
                    slug: gameAffiliate.slug,
                    date: new Date(),
                    price: dataPrice,
                    asin: gameAffiliate.asin
                  });
                  await price.save(async (error, result) => {
                    if (error) {
                      reject(result);
                    }
                    console.log(result);
                    resolve(result);
                  });
                }))
              }
            })
          }
        });
      } catch (err) {
        console.log(err);
      }
      await wait(1000);
    }

    await Promise.all(pricePromises).then(async () => {
      console.log('Waiting 2 minutes to finish everything.');
      await wait(120000);
      mongoose.connection.close();
    })
    .catch(error => console.error(`Error while executing: ${error}`));
  } catch (error) {
    console.log(error);
  }
};

main();

async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}