'use strict'; 

const fs = require('fs');
const axios = require('axios');
const cheerio = require('cheerio');
const { performance } = require('perf_hooks');
const mongoose = require('mongoose');

const mongooseConnectionString = 'mongodb://dev:qnx75me6qspl3xo5@SG-nintendb-35543.servers.mongodirector.com:27017/nintendeal';
mongoose.connect(mongooseConnectionString, { useNewUrlParser: true, useUnifiedTopology: true });

mongoose.connection.on("connected", function() {
  console.log("Connected to database");
});

mongoose.connection.on("error", function(error) {
  console.log("Connection to database failed:" + error);
});

mongoose.connection.on("disconnected", function() {
  console.log("Disconnected from database");
});

var gameSchema = new mongoose.Schema({
  eshop_id: String,
  title: String,
  url: String,
  description: String,
  slug: String,
  box_art: String,
  release_date: String,
  esrb: String,
  virtual_console: String,
  locale: String,
  players: String,
  price_range: String,
  nsuid: String,
  developers: String,
  publishers: String,
  platform: String,
  type: String,
  categories: [String],
  screenshots: [String],
  missing_page: Boolean,
}, {collection: 'games'
});

let gamesInDatabase = new Map();
getGameInDatabase();

let gameCorrections = new Map();
getCorrectedGameData();

let gameErrors = new Map();
let currentSessionPushedToDatabase = new Map();

let bySearchFilesList = new Array();
fs.readdirSync('../eshop-pull-games/games/crawler/by-search-type-1').forEach(file => {
  if (file !== '.DS_Store') {
    const filename = file.split('.json')[0];
    bySearchFilesList.push(filename);
  }
});

setTimeout( () => {
  main();
}, 3000);

let counter = 1;
let pushedToDatabaseCounter = 0;

async function main () {
  let startTime = performance.now();

  await pushGamesFromEShopFile('nintendo-switch-eshop-api/1');
  await pushGamesFromEShopFile('nintendo-switch-eshop-api/2');
  await pushGamesFromEShopFile('nintendo-switch-eshop-api/3');
  await pushGamesFromEShopFile('nintendo-switch-eshop-api/4');
  await pushGamesFromEShopFile('nintendo-switch-eshop-api/5');
  await pushGamesFromEShopFile('nintendo-switch-eshop-api/6');
  await pushGamesFromEShopFile('nintendo-switch-eshop-api/7');
  await pushGamesFromEShopFile('nintendo-switch-eshop-api/8');
  await pushGamesFromEShopFile('nintendo-switch-eshop-api/9');
  await pushGamesFromEShopFile('nintendo-switch-eshop-api/10');
  await pushGamesFromEShopFile('nintendo-switch-eshop-api/11');
  await pushGamesFromEShopFile('nintendo-switch-eshop-api/12');
  await pushGamesFromEShopFile('nintendo-switch-eshop-api/13');
  await pushGamesFromEShopFile('nintendo-switch-eshop-api/14');

  await pushGamesFromEShopFile('crawler/categories/Action');
  await pushGamesFromEShopFile('crawler/categories/Adventure');
  await pushGamesFromEShopFile('crawler/categories/Application');
  await pushGamesFromEShopFile('crawler/categories/Education');
  await pushGamesFromEShopFile('crawler/categories/Fitness');
  await pushGamesFromEShopFile('crawler/categories/Indie');
  await pushGamesFromEShopFile('crawler/categories/Music');
  await pushGamesFromEShopFile('crawler/categories/Party');
  await pushGamesFromEShopFile('crawler/categories/Puzzle');
  await pushGamesFromEShopFile('crawler/categories/Racing');
  await pushGamesFromEShopFile('crawler/categories/Role-Playing');
  await pushGamesFromEShopFile('crawler/categories/Simulation');
  await pushGamesFromEShopFile('crawler/categories/Sports');
  await pushGamesFromEShopFile('crawler/categories/Strategy');

  await pushGamesFromEShopFile('crawler/az/0');
  await pushGamesFromEShopFile('crawler/az/1');
  await pushGamesFromEShopFile('crawler/az/2');
  await pushGamesFromEShopFile('crawler/az/3');
  await pushGamesFromEShopFile('crawler/az/4');
  await pushGamesFromEShopFile('crawler/az/5');
  await pushGamesFromEShopFile('crawler/az/6');
  await pushGamesFromEShopFile('crawler/az/7');
  await pushGamesFromEShopFile('crawler/az/8');
  await pushGamesFromEShopFile('crawler/az/9');
  await pushGamesFromEShopFile('crawler/az/10');
  await pushGamesFromEShopFile('crawler/az/11');
  await pushGamesFromEShopFile('crawler/az/12');
  await pushGamesFromEShopFile('crawler/az/13');
  await pushGamesFromEShopFile('crawler/az/14');
  await pushGamesFromEShopFile('crawler/az/15');
  await pushGamesFromEShopFile('crawler/az/16');
  await pushGamesFromEShopFile('crawler/az/17');
  await pushGamesFromEShopFile('crawler/az/18');
  await pushGamesFromEShopFile('crawler/az/19');
  await pushGamesFromEShopFile('crawler/az/20');
  await pushGamesFromEShopFile('crawler/az/21');
  await pushGamesFromEShopFile('crawler/az/22');
  await pushGamesFromEShopFile('crawler/az/23');

  await pushGamesFromEShopFile('crawler/za/0');
  await pushGamesFromEShopFile('crawler/za/1');
  await pushGamesFromEShopFile('crawler/za/2');
  await pushGamesFromEShopFile('crawler/za/3');
  await pushGamesFromEShopFile('crawler/za/4');
  await pushGamesFromEShopFile('crawler/za/5');
  await pushGamesFromEShopFile('crawler/za/6');
  await pushGamesFromEShopFile('crawler/za/7');
  await pushGamesFromEShopFile('crawler/za/8');
  await pushGamesFromEShopFile('crawler/za/9');
  await pushGamesFromEShopFile('crawler/za/10');
  await pushGamesFromEShopFile('crawler/za/11');
  await pushGamesFromEShopFile('crawler/za/12');
  await pushGamesFromEShopFile('crawler/za/13');
  await pushGamesFromEShopFile('crawler/za/14');
  await pushGamesFromEShopFile('crawler/za/15');
  await pushGamesFromEShopFile('crawler/za/16');
  await pushGamesFromEShopFile('crawler/za/17');
  await pushGamesFromEShopFile('crawler/za/18');
  await pushGamesFromEShopFile('crawler/za/19');
  await pushGamesFromEShopFile('crawler/za/20');
  await pushGamesFromEShopFile('crawler/za/21');
  await pushGamesFromEShopFile('crawler/za/22');
  await pushGamesFromEShopFile('crawler/za/23');

  await pushGamesFromEShopFile('crawler/featured/0');
  await pushGamesFromEShopFile('crawler/featured/1');
  await pushGamesFromEShopFile('crawler/featured/2');
  await pushGamesFromEShopFile('crawler/featured/3');
  await pushGamesFromEShopFile('crawler/featured/4');
  await pushGamesFromEShopFile('crawler/featured/5');
  await pushGamesFromEShopFile('crawler/featured/6');
  await pushGamesFromEShopFile('crawler/featured/7');
  await pushGamesFromEShopFile('crawler/featured/8');
  await pushGamesFromEShopFile('crawler/featured/9');
  await pushGamesFromEShopFile('crawler/featured/10');
  await pushGamesFromEShopFile('crawler/featured/11');
  await pushGamesFromEShopFile('crawler/featured/12');
  await pushGamesFromEShopFile('crawler/featured/13');
  await pushGamesFromEShopFile('crawler/featured/14');
  await pushGamesFromEShopFile('crawler/featured/15');
  await pushGamesFromEShopFile('crawler/featured/16');
  await pushGamesFromEShopFile('crawler/featured/17');
  await pushGamesFromEShopFile('crawler/featured/18');
  await pushGamesFromEShopFile('crawler/featured/19');
  await pushGamesFromEShopFile('crawler/featured/20');
  await pushGamesFromEShopFile('crawler/featured/21');
  await pushGamesFromEShopFile('crawler/featured/22');
  await pushGamesFromEShopFile('crawler/featured/23');

  await pushGamesFromEShopFile('crawler/price-asc/0');
  await pushGamesFromEShopFile('crawler/price-asc/1');
  await pushGamesFromEShopFile('crawler/price-asc/2');
  await pushGamesFromEShopFile('crawler/price-asc/3');
  await pushGamesFromEShopFile('crawler/price-asc/4');
  await pushGamesFromEShopFile('crawler/price-asc/5');
  await pushGamesFromEShopFile('crawler/price-asc/6');
  await pushGamesFromEShopFile('crawler/price-asc/7');
  await pushGamesFromEShopFile('crawler/price-asc/8');
  await pushGamesFromEShopFile('crawler/price-asc/9');
  await pushGamesFromEShopFile('crawler/price-asc/10');
  await pushGamesFromEShopFile('crawler/price-asc/11');
  await pushGamesFromEShopFile('crawler/price-asc/12');
  await pushGamesFromEShopFile('crawler/price-asc/13');
  await pushGamesFromEShopFile('crawler/price-asc/14');
  await pushGamesFromEShopFile('crawler/price-asc/15');
  await pushGamesFromEShopFile('crawler/price-asc/16');
  await pushGamesFromEShopFile('crawler/price-asc/17');
  await pushGamesFromEShopFile('crawler/price-asc/18');
  await pushGamesFromEShopFile('crawler/price-asc/19');
  await pushGamesFromEShopFile('crawler/price-asc/20');
  await pushGamesFromEShopFile('crawler/price-asc/21');
  await pushGamesFromEShopFile('crawler/price-asc/22');
  await pushGamesFromEShopFile('crawler/price-asc/23');

  await pushGamesFromEShopFile('crawler/price-desc/0');
  await pushGamesFromEShopFile('crawler/price-desc/1');
  await pushGamesFromEShopFile('crawler/price-desc/2');
  await pushGamesFromEShopFile('crawler/price-desc/3');
  await pushGamesFromEShopFile('crawler/price-desc/4');
  await pushGamesFromEShopFile('crawler/price-desc/5');
  await pushGamesFromEShopFile('crawler/price-desc/6');
  await pushGamesFromEShopFile('crawler/price-desc/7');
  await pushGamesFromEShopFile('crawler/price-desc/8');
  await pushGamesFromEShopFile('crawler/price-desc/9');
  await pushGamesFromEShopFile('crawler/price-desc/10');
  await pushGamesFromEShopFile('crawler/price-desc/11');
  await pushGamesFromEShopFile('crawler/price-desc/12');
  await pushGamesFromEShopFile('crawler/price-desc/13');
  await pushGamesFromEShopFile('crawler/price-desc/14');
  await pushGamesFromEShopFile('crawler/price-desc/15');
  await pushGamesFromEShopFile('crawler/price-desc/16');
  await pushGamesFromEShopFile('crawler/price-desc/17');
  await pushGamesFromEShopFile('crawler/price-desc/18');
  await pushGamesFromEShopFile('crawler/price-desc/19');
  await pushGamesFromEShopFile('crawler/price-desc/20');
  await pushGamesFromEShopFile('crawler/price-desc/21');
  await pushGamesFromEShopFile('crawler/price-desc/22');
  await pushGamesFromEShopFile('crawler/price-desc/23');

  await pushGamesFromEShopFile('crawler/release-date/0');
  await pushGamesFromEShopFile('crawler/release-date/1');
  await pushGamesFromEShopFile('crawler/release-date/2');
  await pushGamesFromEShopFile('crawler/release-date/3');
  await pushGamesFromEShopFile('crawler/release-date/4');
  await pushGamesFromEShopFile('crawler/release-date/5');
  await pushGamesFromEShopFile('crawler/release-date/6');
  await pushGamesFromEShopFile('crawler/release-date/7');
  await pushGamesFromEShopFile('crawler/release-date/8');
  await pushGamesFromEShopFile('crawler/release-date/9');
  await pushGamesFromEShopFile('crawler/release-date/10');
  await pushGamesFromEShopFile('crawler/release-date/11');
  await pushGamesFromEShopFile('crawler/release-date/12');
  await pushGamesFromEShopFile('crawler/release-date/13');
  await pushGamesFromEShopFile('crawler/release-date/14');
  await pushGamesFromEShopFile('crawler/release-date/15');
  await pushGamesFromEShopFile('crawler/release-date/16');
  await pushGamesFromEShopFile('crawler/release-date/17');
  await pushGamesFromEShopFile('crawler/release-date/18');
  await pushGamesFromEShopFile('crawler/release-date/19');
  await pushGamesFromEShopFile('crawler/release-date/20');
  await pushGamesFromEShopFile('crawler/release-date/21');
  await pushGamesFromEShopFile('crawler/release-date/22');
  await pushGamesFromEShopFile('crawler/release-date/23');

  for (const bySearchFile of bySearchFilesList) {
    await pushGamesFromEShopFile(`crawler/by-search-type-1/${bySearchFile}`);
  }

  let finishTime = performance.now();
  let elapsedTime = ((finishTime - startTime) / 1000) / 60;
  console.log(`\nUpdating games done. Took ${elapsedTime} minutes.`);
  mongoose.connection.close();
}

async function pushGamesFromEShopFile(file) {
  console.log(`\nUpdating games from ${file} file.`);
  let rawdata = fs.readFileSync(`../eshop-pull-games/games/${file}.json`);
  let gamesByCategory = JSON.parse(rawdata);

  for (const page of gamesByCategory) {
    for (let game of page) {
      let title = game['title'];
      
      if (gameCorrections.has(game['nsuid'])) {
        game = gameCorrections.get(game['nsuid']);
      }

      console.log(`[ Game \x1b[32m${counter}\x1b[0m | \x1b[32m${pushedToDatabaseCounter}\x1b[0m pushed ]   Game [ \x1b[32m${title}\x1b[0m ] found.`);
      
      if (game['nsuid'] && game['nsuid'] !== undefined && game['nsuid'].length > 0) {
        if (gamesInDatabase.has(game['slug']) || currentSessionPushedToDatabase.has(game['nsuid'])) {
          console.log(`                              Game [ \x1b[32m${title}\x1b[0m ] already exists in games table.`);
        } else {
          const gameModel = mongoose.model('Game', gameSchema);
          const g = await createGameModel(game, gameModel);
          let saved = await g.save();
          console.log(`                              Game [ \x1b[32m${title}\x1b[0m ] pushed to games table.`);
          currentSessionPushedToDatabase.set(game['nsuid'], true);
          pushedToDatabaseCounter++;
          fs.appendFileSync('./added', game['title'] + ' : ' + game['nsuid'] + '\n');
        }
      }
      counter++;
    }
  }

  console.log(`\nGames from ${file} file done.`);
}

async function createGameModel(game, gameModel) {
  let gamePageUrl =  'https://www.nintendo.com' + game['url'];
  let gamePageData = await scrapeGamePage(gamePageUrl);

  let screenshots = await pushGameScreenshots(game, gamePageData);

  return new gameModel({
    eshop_id: game['objectID'],
    title: game['title'],
    url: game['url'],
    description: game['description'],
    slug: game['slug'],
    box_art: game['boxArt'],
    release_date: game['releaseDateMask'],
    esrb: game['esrb'],
    virtual_console: game['virtualConsole'],
    locale: game['locale'],
    players: game['players'],
    price_range: game['priceRange'],
    nsuid: game['nsuid'],
    developers: await getDevelopers(game),
    publishers: await getPublishers(game),
    platform: game['platform'],
    type: game['type'],
    categories: game['categories'],
    screenshots: screenshots,
    missing_page: (gamePageData === undefined) ? true : false
  });
}

async function getDevelopers(game) {
  if (game['developers'] === undefined) {
    return null;
  } else {
    let count = 0;
    let developers = '';
    for (const developer of game['developers']) {
      if (count < 1) {
        developers += `${developer}`;
      } else {
        developers += `,${developer}`;
      }
      count++;
    }
    return developers;
  }
}

async function getPublishers(game) {
  if (game['publishers'] === undefined) {
    return null;
  } else {
    let count = 0;
    let publishers = '';
    for (const publisher of game['publishers']) {
      if (count < 1) {
        publishers += `${publisher}`;
      } else {
        publishers += `,${publisher}`;
      }
      count++;
    }
    return publishers;
  }
}

async function pushGameScreenshots(game, gamePageData) {
  let gamePageUrl =  'https://www.nintendo.com' + game['url'];
  const indent = '          ';

  let finalScreenshotUrls = [];
  if (gamePageData !== undefined) {
    let screenshotUrls = await getScreenshotUrls(gamePageData);

    for (const screenshotUrl of screenshotUrls) {
      if (screenshotUrl !== undefined) {
        finalScreenshotUrls.push(screenshotUrl);
      } else {
        if (!gameErrors.has(game['nsuid'])) {
          gameErrors.set(game['nsuid'], true);
          fs.appendFileSync('./errors.log', game['title'] + ':' + gamePageUrl + '\n');
          console.log(`${indent}\x1b[31mA screenshot url is undefined.\x1b[0m`);
        }
      }
    }
  } else {
    if (!gameErrors.has(game['nsuid'])) {
      gameErrors.set(game['nsuid'], true);
      fs.appendFileSync('./errors.log', game['title'] + ':' + gamePageUrl + '\n');
      console.log(`${indent}\x1b[31mIssues with screenshots. Maybe not found.\x1b[0m`);
    }
  }
  return finalScreenshotUrls;
}

async function scrapeGamePage(url) {
  try {
    let result = await axios.get(url);
    return result.data;
  } catch (error) {
    const indent = '          ';
    console.log(`${indent}\x1b[31mError trying to scrape game page.\x1b[0m`);
    console.log(`${indent}\x1b[31mUrl: ${url}\x1b[0m`);
  }
}

async function getScreenshotUrls(scrapeData) {
  const $ = cheerio.load(scrapeData);
  let screenshotUrls = [];
  $('#gallery product-gallery-item[type=image]').map( async (index, element) => {
    screenshotUrls.push($(element).attr('src'));
  });

  return screenshotUrls;
}

async function getCorrectedGameData() {
  const correctedData = fs.readFileSync('corrected_data.json', {encoding:'utf8', flag:'r'});
  const correctedDataParsed = JSON.parse(correctedData);

  for (const game of correctedDataParsed) {
    gameCorrections.set(game.nsuid, game.corrected[0]);
  }
}

async function getGameInDatabase() {
  const games = fs.readFileSync('../scratchpad/database-pull-all-games/slugs', {encoding:'utf8', flag:'r'}).split('\n');
  
  for (const game of games) {
    gamesInDatabase.set(game, true);
  }
}

async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}