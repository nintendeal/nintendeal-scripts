'use strict';

const fs = require('fs');
const axios = require('axios');
const cheerio = require('cheerio');
const { Pool, Client } = require('pg')
const { performance } = require('perf_hooks');

async function createPgPool() {
  return new Pool({
    user: 'doadmin',
    host: 'nintendeal-db-postgresql-do-user-2547760-0.db.ondigitalocean.com',
    database: 'nintendeal',
    password: 'qnx75me6qspl3xo5',
    port: 25060,
    ssl: true,
    rejectUnauthorized: false
  })
}

main();

async function main () {
  let startTime = performance.now();

  await pushGamesByCategoryFile('Action');
  await pushGamesByCategoryFile('Adventure');
  await pushGamesByCategoryFile('Application');
  await pushGamesByCategoryFile('Education');
  await pushGamesByCategoryFile('Fitness');
  await pushGamesByCategoryFile('Indie');
  await pushGamesByCategoryFile('Music');
  await pushGamesByCategoryFile('Party');
  await pushGamesByCategoryFile('Puzzle');
  await pushGamesByCategoryFile('Racing');
  await pushGamesByCategoryFile('Role-Playing');
  await pushGamesByCategoryFile('Simulation');
  await pushGamesByCategoryFile('Sports');
  await pushGamesByCategoryFile('Strategy');

  let finishTime = performance.now();
  let elapsedTime = (finishTime - startTime) / 1000;
  console.log(`\nUpdating games done. Took ${elapsedTime} seconds.`);
}

async function pushGamesByCategoryFile(file) {
  let count = 1;
  console.log(`\nUpdating games from ${file} file.`);
  let rawdata = fs.readFileSync(`../eshop-pull-games/games/${file}.json`);
  let gamesByCategory = JSON.parse(rawdata);

  for (const page of gamesByCategory) {
    const pool = await createPgPool();
    for (const game of page) {
      let title = game['title'];
      if ((await isGameInDB(pool, game))) {
        console.log(`[${count}] Game [ \x1b[32m${title}\x1b[0m ] already exists in games table.`);
      } else {
        console.log(`[${count}] Pushing game [ \x1b[32m${title}\x1b[0m ].`);
        await pushGameInformation(pool, game);
        await pushGameCategories(pool, game);
        await pushGameScreenshots(pool, game);
      }
      count++;
    }
    pool.end();
  }

  console.log(`\nGames from ${file} file done.`);
}

async function isGameInDB(pool, game) {
  const text = 'SELECT id, title FROM public.games WHERE nsuid = $1';
  const values = [game['nsuid']];

  return await pool
    .connect()
    .then(async(client) => {
      return await client
        .query(text, values)
        .then(async(res) => {
          await client.release();
          if (res.rows.length > 0) {
            return true;
          } else {
            return false;
          }
        })
        .catch(async(err) => {
          await client.release()
          console.log(err.stack)
        })
    })
}

async function pushGameInformation(pool, game) {
  const indent = '          ';
  console.log(`${indent}Pushing game information to games table.`);

  const query = await dbInsertQuery();
  const values = await dbInsertValues(game);
  await executeSqlQuery(pool, query, values);
}

async function dbInsertQuery() {
  return 'INSERT INTO public.games\
          (eshop_id, title, url, description, slug, box_art, release_date, \
          esrb, virtual_console, locale, players, \
          price_range, nsuid, developers, publishers, platform, type) \
          VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, \
          $10, $11, $12, $13, $14, $15, $16, $17) RETURNING *';
}

async function dbInsertValues(game) {
  const developers = await getDevelopers(game);
  const publishers = await getPublishers(game);

  return [
    game['objectID'],
    game['title'],
    game['url'],
    game['description'],
    game['slug'],
    game['boxArt'],
    game['releaseDateMask'],
    game['esrb'],
    game['virtualConsole'],
    game['locale'],
    game['players'],
    game['priceRange'],
    game['nsuid'],
    developers,
    publishers,
    game['platform'],
    game['type']
  ];
}

async function getDevelopers(game) {
  if (game['developers'] === undefined) {
    return null;
  } else {
    let count = 0;
    let developers = '';
    for (const developer of game['developers']) {
      if (count < 1) {
        developers += `${developer}`;
      } else {
        developers += `,${developer}`;
      }
      count++;
    }
    return developers;
  }
}

async function getPublishers(game) {
  if (game['publishers'] === undefined) {
    return null;
  } else {
    let count = 0;
    let publishers = '';
    for (const publisher of game['publishers']) {
      if (count < 1) {
        publishers += `${publisher}`;
      } else {
        publishers += `,${publisher}`;
      }
      count++;
    }
    return publishers;
  }
}

async function pushGameCategories(pool, game) {
  const indent = '          ';
  console.log(`${indent}Pushing game categories to games_categories table.`);

  let gameId = await getGameDBId(pool, game);
  let categoryToId = await mapCategoriesToId();

  for (const category of game['categories']) {
    let categoryId = categoryToId.get(category);
    if (categoryId === undefined) {
      fs.appendFileSync('./missing-categories.log', category + '\n');
      console.log(`${indent}\x1b[31mMissing category: ${category}.\x1b[0m`);
    } else {
      const text = 'INSERT INTO public.games_categories\
                  (game_id, category_id) VALUES($1, $2)';
      const values = [gameId, categoryId];
    
      await pool
        .connect()
        .then(async(client) => {
          return await client
            .query(text, values)
            .then(async(res) => {
              await client.release();
            })
            .catch(async(err) => {
              await client.release()
              console.log(err.stack)
            })
        })
    }
  }

}

async function getGameDBId(pool, game) {
  const text = 'SELECT id FROM public.games WHERE nsuid = $1';
  const values = [game['nsuid']];

  return await pool
    .connect()
    .then(async(client) => {
      return await client
        .query(text, values)
        .then(async(res) => {
          await client.release();
          return res.rows[0].id;
        })
        .catch(async(err) => {
          await client.release()
          console.log(err.stack)
        })
    });
}

async function mapCategoriesToId() {
  return new Map([
    ["Action", 1],
    ["Adventure", 2],
    ["Application", 3],
    ["Education", 4],
    ["Fitness", 5],
    ["Indie", 6],
    ["Music", 7],
    ["Party", 8],
    ["Puzzle", 9],
    ["Racing", 10],
    ["Role-Playing", 11],
    ["Simulation", 12],
    ["Sports", 13],
    ["Strategy", 14],
    ["Other", 15],
    ["Multiplayer", 16],
    ["Arcade", 17],
    ["Updates", 18],
    ["First-Person", 19],
    ["Platformer", 20],
    ["Fighting", 21],
    ["Communication", 22],
    ["Training", 23],
    ["Shooter", 24],
    ["Board Game", 25],
    ["Lifestyle", 26],
    ["Study", 27],
    ["Practical", 28],
    ["Utility", 29],
    ["first-person-shooter", 30],
    ["Video", 31]
  ]);
}

async function pushGameScreenshots(pool, game) {
  const indent = '          ';
  console.log(`${indent}Pushing game screenshots to games_screenshots table.`);

  let gamePageUrl =  'https://www.nintendo.com' + game['url'];
  let gamePageData = await scrapeGamePage(gamePageUrl);

  if (gamePageData !== undefined) {
    let screenshotUrls = await getScreenshotUrls(gamePageData);

    for (const screenshotUrl of screenshotUrls) {
      if (screenshotUrl !== undefined) {
        let gameId = await getGameDBId(pool, game);
        const query = 'INSERT INTO public.games_screenshots\
                    (game_id, screenshot_url) VALUES($1, $2)';
        const values = [gameId, screenshotUrl];
        await executeSqlQuery(pool, query, values);
      } else {
        fs.appendFileSync('./screenshot-errors.log', game['title'] + ':' + gamePageUrl + '\n');
        console.log(`${indent}\x1b[31mIssues with pushing screenshots.\x1b[0m`);
      }
    }
  } else {
    fs.appendFileSync('./screenshot-errors.log', game['title'] + ':' + gamePageUrl + '\n');
    console.log(`${indent}\x1b[31mIssues with pushing screenshots.\x1b[0m`);
  }
}

async function scrapeGamePage(url) {
  try {
    let result = await axios.get(url);
    return result.data;
  } catch (error) {
    const indent = '          ';
    console.log(`${indent}\x1b[31mError trying to scrape screenshots.\x1b[0m`);
    console.log(`${indent}\x1b[31mUrl: ${url}\x1b[0m`);
  }
}

async function getScreenshotUrls(scrapeData) {
  const $ = cheerio.load(scrapeData);
  let screenshotUrls = [];
  $('#gallery img').map( async (index, element) => {
    screenshotUrls.push($(element).attr('data-src'));
  });

  return screenshotUrls;
}

async function executeSqlQuery(pool, query, values) {
  await pool
    .connect()
    .then(async(client) => {
      return await client
        .query(query, values)
        .then(async(res) => {
          await client.release();
        })
        .catch(async(err) => {
          await client.release()
          console.log(err.stack)
        })
    })
}